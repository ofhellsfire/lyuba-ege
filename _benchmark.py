#!/usr/bin/env python3

import importlib
import sys


solution = None

try:
    solution = sys.argv[1]
except:
    pass
finally:
    if solution not in ('naive', 'optimal', 'best'):
        solution = 'best'

get_count = importlib.import_module(solution).get_count


def get_numbers():
    with open('sample.txt') as f:
        for line in f:
            yield int(line)


result = get_count(1000, get_numbers())
print(result)

