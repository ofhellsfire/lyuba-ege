import importlib
import unittest


get_count = importlib.import_module('best').get_count


class Tests(unittest.TestCase):

    def test_empty(self):
        count = get_count(0, [])
        self.assertEqual(0, count)

    def test_single_value(self):
        count = get_count(1, [7])
        self.assertEqual(0, count)

    def test_2_4_values(self):
        count = get_count(4, [7, 1, 2, 3])
        self.assertEqual(0, count)

    def test_single_left(self):
        count = get_count(12, [7, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12])
        self.assertEqual(8, count)

    def test_single_left_shift_by_1(self):
        count = get_count(12, [1, 7, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12])
        self.assertEqual(7, count)

    def test_single_left_shift_by_2(self):
        count = get_count(12, [1, 2, 7, 3, 4, 5, 6, 8, 9, 10, 11, 12])
        self.assertEqual(6, count)

    def test_single_left_shift_by_3(self):
        count = get_count(12, [1, 2, 3, 7, 4, 5, 6, 8, 9, 10, 11, 12])
        self.assertEqual(5, count)

    def test_single_right(self):
        count = get_count(13, [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 7])
        self.assertEqual(9, count)

    def test_single_right_shift_by_1(self):
        count = get_count(13, [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 7, 13])
        self.assertEqual(8, count)

    def test_single_right_shift_by_2(self):
        count = get_count(13, [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 7, 12, 13])
        self.assertEqual(7, count)

    def test_single_right_shift_by_3(self):
        count = get_count(13, [1, 2, 3, 4, 5, 6, 8, 9, 10, 7, 11, 12, 13])
        self.assertEqual(6, count)

    def test_single_middle(self):
        count = get_count(12, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        self.assertEqual(5, count)

    def test_double_left_middle(self):
        count = get_count(15, [7, 1, 2, 3, 4, 5, 6, 14, 8, 9, 10, 11, 12, 13, 15])
        self.assertEqual(18, count)

    def test_double_middle_middle(self):
        count = get_count(15, [1, 2, 3, 4, 5, 14, 6, 8, 9, 7, 10, 11, 12, 13, 15])
        self.assertEqual(15, count)

    def test_double_middle_right(self):
        count = get_count(15, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 14])
        self.assertEqual(18, count)

    def test_all_values(self):
        """
        [7, 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91, 98, 105]

        1) 7-35 2) 7-42 3) 7-49 4) 7-56 5) 7-63 6) 7-70 7) 7-84 8) 7-91 9) 7-98
        10) 7-105 11) 14-42 12) 14-49 13) 14-56 14) 14-63 15) 14-70 16) 14-77
        17) 14-84 18) 14-91 19) 14-98 20) 14-105 21) 21-49 22) 21-56 23) 21-63
        24) 21-70 25) 21-77 26) 21-84 27) 21-91 28) 21-98 29) 21-105 30) 28-56
        31) 28-63 32) 28-70 33) 28-77 34) 28-84 35) 28-91 36) 28-98 37) 28-105
        38) 35-63 39) 35-70 40) 35-77 41) 35-84 42) 35-91 43) 35-98 44) 35-105
        45) 42-70 46) 42-77 47) 42-84 48) 42-91 49) 42-98 50) 42-105 51) 49-77
        52) 49-84 53) 49-91 54) 49-98 55) 49-105 56) 56-84 57) 56-91 58) 56-98
        59) 56-105 60) 63-91 61) 63-98 62) 63-105 63) 70-98 64) 70-105
        65) 77-105 66) 7-77

        """
        count = get_count(15, [7, 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91, 98, 105])
        self.assertEqual(66, count)

    def test_three_middle(self):
        """
        [1, 2, 3, 4, 7, 5, 6, 8, 14, 15, 9, 10, 21, 11, 12, 13, 16]

        1) 7-1 2) 7-14 3) 7-15 4) 7-9 5) 7-10 6) 7-21 7) 7-11 8) 7-12 9) 7-13
        10) 7-16 11) 14-1 12) 14-2 13) 14-3 14) 14-4 15) 14-21 16) 14-11
        17) 14-12 18) 14-13 19) 14-16 20) 21-1 21) 21-2 22) 21-3 23) 21-4
        24) 21-5 25) 21-6 26) 21-8 27) 21-16

        """
        count = get_count(17, [1, 2, 3, 4, 7, 5, 6, 8, 14, 15, 9, 10, 21, 11, 12, 13, 16])
        self.assertEqual(27, count)

    def test_tricky(self):
        """
        [7, 1, 14, 21, 2, 3, 4, 28, 5, 35, 42, 6, 10, 8, 49, 56, 63, 9]

        1) 7-2 2) 7-3 3) 7-4 4) 7-28 5) 7-5 6) 7-35 7) 7-42 8) 7-6 9) 7-10 10) 7-8
        11) 7-49 12) 7-56 13) 7-63 14) 7-9 15) 14-4 16) 14-28 17) 14-5 18) 14-35
        19) 14-42 20) 14-6 21) 14-10 22) 14-8 23) 14-49 24) 14-56 25) 14-63 26) 14-9
        27) 21-28 28) 21-5 29) 21-35 30) 21-42 31) 21-6 32) 21-10 33) 21-8 34) 21-49
        35) 21-56 36) 21-63 37) 21-9 38) 28-1 39) 28-6 40) 28-10 41) 28-8 42) 28-49
        43) 28-56 44) 28-63 45) 28-9 46) 35-1 47) 35-2 48) 35-3 49) 35-8 50) 35-49
        51) 35-56 52) 35-63 53) 35-9 54) 42-1 55) 42-2 56) 42-3 57) 42-4 58) 42-49
        59) 42-56 60) 42-63 61) 42-9 62) 49-1 63) 49-2 64) 49-3 65) 49-4 66) 49-5
        67) 56-1 68) 56-2 69) 56-3 70) 56-4 71) 56-5 72) 56-6 73) 63-1 74) 63-2
        75) 63-3 76) 63-4 77) 63-5 78) 63-6 79) 63-10
        """
        count = get_count(18, [7, 1, 14, 21, 2, 3, 4, 28, 5, 35, 42, 6, 10, 8, 49, 56, 63, 9])
        self.assertEqual(79, count)


if __name__ == '__main__':
    unittest.main()

