def norm_slice(slize):
    if slize.stop < 0:
        return slice(None, 0)
    return slize


def get_count(length, seq, divider=7, gap=4):
    s = list(seq)
    result = set()
    for index, x in enumerate(s):
        new_slice = norm_slice(slice(None, index - gap))
        sub_seq = s[new_slice] + s[index + gap:]
        for y in sub_seq:
            if x * y % divider == 0:
                result.add(tuple(sorted((x, y))))
    return len(result)
