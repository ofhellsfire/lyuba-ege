from collections import deque


def limit(value):
    """Returns value if positive, otherwise 0
    """
    return value if value > 0 else 0


def get_count(length, seq, divider=7, gap=3):
    if length <= gap + 1:
        return 0

    count = 0
    found_count = 0
    discard = 0
    frame = gap + 1
    buf = deque()
    for index, val in enumerate(seq):
        if index > gap:
            buf.popleft()
        buf.append(val)
        if val % divider == 0:
            if index < frame:  # left
                count += (length - 1) - gap - index
            else:
                discard = limit(sum(int(not (i % divider)) for i in buf) - 1)
                if length - index <= frame:  # right
                    count += index - gap - found_count + discard
                else:  # middle
                    count += (length - 1) - 2 * gap - found_count + discard
            found_count += 1
    return count

