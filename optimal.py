def get_count(length, seq, divider=7, gap=3):
    s = list(seq)
    if length <= gap + 1:
        return 0
    count = 0
    last_x_divisible_count = 0
    known_divisible_count = 0
    for index, val in enumerate(s):
        if val % divider == 0:
            last_x_divisible_count = sum(int(not (i % divider)) for i in s[index - gap:index])
            if index < gap:
                count += (length - 1) - gap - index
            elif index > (length - 1) - gap:
                count += index - gap - known_divisible_count + last_x_divisible_count
            else:
                count += (length - 1) - 2 * gap - known_divisible_count + last_x_divisible_count
            known_divisible_count += 1
    return count
