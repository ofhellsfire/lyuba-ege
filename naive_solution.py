#from read_seq import read
import random

# seq = list(read('sequence.txt'))

# Naive
#@profile
def get_count_naive(seq, divider=7):
    count = 0
    cycles = 0
    for i in range(len(seq)):
        for y in range(i + 4, len(seq)):
            cycles += 1
#            print(f'{cycles}: Pair "{seq[i]}" & "{seq[y]}": Mul = {seq[i] * seq[y]}')
            if seq[i] * seq[y] % divider == 0:
#                print(f'Pair "{seq[i]}" & "{seq[y]}" is divisible by {divider}')
                count += 1
    return count, cycles


# Time Optimal
#@profile
def get_count_time_optimal(seq, divider=7, gap=3):
    count = 0
    cycles = 0
    for i in range(len(seq)):
        cycles += 1
        if seq[i] % divider == 0:
            if i < (gap + 1):
                count += len(seq) - (gap - 1) - i
            elif i > len(seq) - (gap - 1):
                count += i - gap
            else:
                count += len(seq) - 2 * gap - 1
    return count, cycles



print('Naive: In the Beginning')
seq = [7, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13]
count, cycles = get_count_naive(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')

print('Naive: In the Middle')
seq = [6, 3, 13, 12, 5, 8, 2, 7, 1, 4, 9, 11, 10]
count, cycles = get_count_naive(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')

print('Naive: In the End')
seq = [13, 10, 6, 2, 8, 5, 12, 3, 4, 11, 1, 9, 7]
count, cycles = get_count_naive(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')


print('Optimal: In the Beginning')
seq = [7, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13]
count, cycles = get_count_time_optimal(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')

print('Optimal: In the Middle')
seq = [6, 3, 13, 12, 5, 8, 2, 7, 1, 4, 9, 11, 10]
count, cycles = get_count_time_optimal(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')

print('Optimal: In the End')
seq = [13, 10, 6, 2, 8, 5, 12, 3, 4, 11, 1, 9, 7]
count, cycles = get_count_time_optimal(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')


seq = list(range(1, 15))
random.shuffle(seq)

print('Naive: Random')
count, cycles = get_count_naive(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')

print('Optimal: Random')
count, cycles = get_count_time_optimal(seq)
#assert count == 25
print(seq)
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')

"""
print('Time Optimal')
seq = list(range(1, 30))
count, cycles = get_count_time_optimal(seq)
assert count == 25
print(f'Sequence size = {len(seq)}')
print(f'Number of pairs = {count}')
print(f'Number cycles = {cycles}')
print('')
"""
