# Run Tests

```
python -m unittest -v tests.Tests
```

# Benchmarking

## Naive

### Running Time

```
$ hyperfine --runs 50 --warmup 10 ./naive_benchmark.py
Benchmark #1: ./naive_benchmark.py
  Time (mean ± σ):     362.5 ms ±   2.3 ms    [User: 346.8 ms, System: 19.8 ms]
  Range (min … max):   358.1 ms … 368.8 ms    50 runs
```

### Line Profiling

```
$ kernprof -l -v _benchmark.py naive
115558
Wrote profile results to _benchmark.py.lprof
Timer unit: 1e-06 s

Total time: 1.35647 s
File: naive.py
Function: get_count at line 7

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
     7                                           @profile
     8                                           def get_count(length, seq, divider=7, gap=4):
     9         1        741.0    741.0      0.1      s = list(seq)
    10         1          1.0      1.0      0.0      result = set()
    11      1001        591.0      0.6      0.0      for index, x in enumerate(s):
    12      1000       1418.0      1.4      0.1          new_slice = norm_slice(slice(None, index - gap))
    13      1000       6419.0      6.4      0.5          sub_seq = s[new_slice] + s[index + gap:]
    14    993016     492791.0      0.5     36.3          for y in sub_seq:
    15    992016     584969.0      0.6     43.1              if x * y % divider == 0:
    16    230882     269535.0      1.2     19.9                  result.add(tuple(sorted((x, y))))
    17         1          2.0      2.0      0.0      return len(result)
```

### Memory Profiling (Num Count: 1,000)

```
$ python -m memory_profiler _benchmark.py naive
115558
Filename: naive.py

Line #    Mem usage    Increment   Line Contents
================================================
     7   14.707 MiB   14.707 MiB   @profile
     8                             def get_count(length, seq, divider=7, gap=4):
     9   14.707 MiB    0.000 MiB       s = list(seq)
    10   14.707 MiB    0.000 MiB       result = set()
    11   26.039 MiB    0.000 MiB       for index, x in enumerate(s):
    12   26.039 MiB    0.000 MiB           new_slice = norm_slice(slice(None, index - gap))
    13   26.039 MiB    0.000 MiB           sub_seq = s[new_slice] + s[index + gap:]
    14   26.039 MiB    0.250 MiB           for y in sub_seq:
    15   26.039 MiB    0.000 MiB               if x * y % divider == 0:
    16   26.039 MiB    2.062 MiB                   result.add(tuple(sorted((x, y))))
    17   26.039 MiB    0.000 MiB       return len(result)
```

## Optimal

### Running Time

```
$ hyperfine --runs 50 --warmup 10 ./optimal_benchmark.py 
Benchmark #1: ./optimal_benchmark.py
  Time (mean ± σ):      63.4 ms ±   0.7 ms    [User: 54.3 ms, System: 13.5 ms]
  Range (min … max):    61.9 ms …  64.7 ms    50 runs
```

### Line Profiling

```
$ kernprof -l -v _benchmark.py optimal
115558
Wrote profile results to _benchmark.py.lprof
Timer unit: 1e-06 s

Total time: 0.00265 s
File: optimal.py
Function: get_count at line 1

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
     1                                           @profile
     2                                           def get_count(length, seq, divider=7, gap=3):
     3         1        786.0    786.0     29.7      s = list(seq)
     4         1          1.0      1.0      0.0      if length <= gap + 1:
     5                                                   return 0
     6         1          1.0      1.0      0.0      count = 0
     7         1          0.0      0.0      0.0      last_x_divisible_count = 0
     8         1          1.0      1.0      0.0      known_divisible_count = 0
     9      1001        568.0      0.6     21.4      for index, val in enumerate(s):
    10      1000        586.0      0.6     22.1          if val % divider == 0:
    11       124        368.0      3.0     13.9              last_x_divisible_count = sum(int(not (i % divider)) for i in s[index - gap:index])
    12       124         78.0      0.6      2.9              if index < gap:
    13                                                           count += (length - 1) - gap - index
    14       124         92.0      0.7      3.5              elif index > (length - 1) - gap:
    15                                                           count += index - gap - known_divisible_count + last_x_divisible_count
    16                                                       else:
    17       124         97.0      0.8      3.7                  count += (length - 1) - 2 * gap - known_divisible_count + last_x_divisible_count
    18       124         71.0      0.6      2.7              known_divisible_count += 1
    19         1          1.0      1.0      0.0      return count
```

### Memory Profiling (Num Count: 100,000)

```
$ python -m memory_profiler _benchmark.py optimal
615322701
Filename: optimal.py

Line #    Mem usage    Increment   Line Contents
================================================
     1   39.633 MiB   39.633 MiB   @profile
     2                             def get_count(length, seq, divider=7, gap=3):
     3   44.145 MiB    4.512 MiB       s = list(seq)
     4   44.145 MiB    0.000 MiB       if length <= gap + 1:
     5                                     return 0
     6   44.145 MiB    0.000 MiB       count = 0
     7   44.145 MiB    0.000 MiB       last_x_divisible_count = 0
     8   44.145 MiB    0.000 MiB       known_divisible_count = 0
     9   44.145 MiB    0.000 MiB       for index, val in enumerate(s):
    10   44.145 MiB    0.000 MiB           if val % divider == 0:
    11   44.145 MiB    0.000 MiB               last_x_divisible_count = sum(int(not (i % divider)) for i in s[index - gap:index])
    12   44.145 MiB    0.000 MiB               if index < gap:
    13   44.145 MiB    0.000 MiB                   count += (length - 1) - gap - index
    14   44.145 MiB    0.000 MiB               elif index > (length - 1) - gap:
    15   44.145 MiB    0.000 MiB                   count += index - gap - known_divisible_count + last_x_divisible_count
    16                                         else:
    17   44.145 MiB    0.000 MiB                   count += (length - 1) - 2 * gap - known_divisible_count + last_x_divisible_count
    18   44.145 MiB    0.000 MiB               known_divisible_count += 1
    19   44.145 MiB    0.000 MiB       return count
```

## Best

### Running Time

```
$ hyperfine --runs 50 --warmup 10 ./best_benchmark.py 
Benchmark #1: ./best_benchmark.py
  Time (mean ± σ):      63.2 ms ±   0.8 ms    [User: 54.6 ms, System: 13.0 ms]
  Range (min … max):    61.4 ms …  67.0 ms    50 runs
```

### Line Profiling

```
$ kernprof -l -v _benchmark.py best
115558
Wrote profile results to _benchmark.py.lprof
Timer unit: 1e-06 s

Total time: 0.00478 s
File: best.py
Function: get_count at line 10

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
    10                                           @profile
    11                                           def get_count(length, seq, divider=7, gap=3):
    12         1          2.0      2.0      0.0      if length <= gap + 1:
    13                                                   return 0
    14                                           
    15         1          1.0      1.0      0.0      count = 0
    16         1          0.0      0.0      0.0      found_count = 0
    17         1          1.0      1.0      0.0      discard = 0
    18         1          1.0      1.0      0.0      frame = gap + 1
    19         1          2.0      2.0      0.0      buf = deque()
    20      1001       1485.0      1.5     31.1      for index, val in enumerate(seq):
    21      1000        601.0      0.6     12.6          if index > gap:
    22       996        642.0      0.6     13.4              buf.popleft()
    23      1000        624.0      0.6     13.1          buf.append(val)
    24      1000        630.0      0.6     13.2          if val % divider == 0:
    25       124         74.0      0.6      1.5              if index < frame:  # left
    26         1          1.0      1.0      0.0                  count += (length - 1) - gap - index
    27                                                       else:
    28       123        455.0      3.7      9.5                  discard = limit(sum(int(not (i % divider)) for i in buf) - 1)
    29       123         80.0      0.7      1.7                  if length - index <= frame:  # right
    30                                                               count += index - gap - found_count + discard
    31                                                           else:  # middle
    32       123        106.0      0.9      2.2                      count += (length - 1) - 2 * gap - found_count + discard
    33       124         74.0      0.6      1.5              found_count += 1
    34         1          1.0      1.0      0.0      return count
```

### Memory Profiling (Num Count: 100,000)

```
$ python -m memory_profiler _benchmark.py best
615322701
Filename: best.py

Line #    Mem usage    Increment   Line Contents
================================================
    10   39.684 MiB   39.684 MiB   @profile
    11                             def get_count(length, seq, divider=7, gap=3):
    12   39.684 MiB    0.000 MiB       if length <= gap + 1:
    13                                     return 0
    14                             
    15   39.684 MiB    0.000 MiB       count = 0
    16   39.684 MiB    0.000 MiB       found_count = 0
    17   39.684 MiB    0.000 MiB       discard = 0
    18   39.684 MiB    0.000 MiB       frame = gap + 1
    19   39.684 MiB    0.000 MiB       buf = deque()
    20   39.684 MiB    0.000 MiB       for index, val in enumerate(seq):
    21   39.684 MiB    0.000 MiB           if index > gap:
    22   39.684 MiB    0.000 MiB               buf.popleft()
    23   39.684 MiB    0.000 MiB           buf.append(val)
    24   39.684 MiB    0.000 MiB           if val % divider == 0:
    25   39.684 MiB    0.000 MiB               if index < frame:  # left
    26   39.684 MiB    0.000 MiB                   count += (length - 1) - gap - index
    27                                         else:
    28   39.684 MiB    0.000 MiB                   discard = limit(sum(int(not (i % divider)) for i in buf) - 1)
    29   39.684 MiB    0.000 MiB                   if length - index <= frame:  # right
    30   39.684 MiB    0.000 MiB                       count += index - gap - found_count + discard
    31                                             else:  # middle
    32   39.684 MiB    0.000 MiB                       count += (length - 1) - 2 * gap - found_count + discard
    33   39.684 MiB    0.000 MiB               found_count += 1
    34   39.684 MiB    0.000 MiB       return count
```

## Benchmark Comparison

TBD
